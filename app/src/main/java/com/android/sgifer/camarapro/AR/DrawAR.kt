package com.android.sgifer.camarapro.AR

/**
 * Created by SGIFER on 23/03/18.
 */

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.Matrix
import android.os.Bundle
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.Toast

import com.google.ar.core.ArCoreApk
import com.google.ar.core.Camera
import com.google.ar.core.Config
import com.google.ar.core.Frame
import com.google.ar.core.Session
import com.google.ar.core.TrackingState
import com.google.ar.core.exceptions.UnavailableApkTooOldException
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException
import com.google.ar.core.exceptions.UnavailableSdkTooOldException
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException
import com.android.sgifer.camarapro.rendering.BackgroundRenderer
import com.android.sgifer.camarapro.rendering.LineShaderRenderer
import com.android.sgifer.camarapro.rendering.LineUtils
import com.android.sgifer.camarapro.R

import java.util.ArrayList
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference

import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import javax.vecmath.Vector2f
import javax.vecmath.Vector3f

class DrawAR : AppCompatActivity(), GLSurfaceView.Renderer, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    private var mSurfaceView: GLSurfaceView? = null

    private val mDefaultConfig: Config? = null
    private var mSession: Session? = null
    private val mBackgroundRenderer = BackgroundRenderer()
    private val mLineShaderRenderer = LineShaderRenderer()
    private var mFrame: Frame? = null

    private val projmtx = FloatArray(16)
    private val viewmtx = FloatArray(16)
    private var mZeroMatrix = FloatArray(16)

    private var mPaused = false

    private var mScreenWidth = 0f
    private var mScreenHeight = 0f

    private var biquadFilter: BiquadFilter? = null
    private var mLastPoint: Vector3f? = null
    private val lastTouch = AtomicReference<Vector2f>()

    private var mDetector: GestureDetectorCompat? = null

    private var mSettingsUI: LinearLayout? = null
    private var mButtonBar: LinearLayout? = null

    private var mLineWidthBar: SeekBar? = null
    private var mLineDistanceScaleBar: SeekBar? = null
    private var mSmoothingBar: SeekBar? = null


    private var mLineWidthMax = 0.33f
    private var mDistanceScale = 0.0f
    private var mLineSmoothing = 0.1f

    private var mLastFramePosition: FloatArray? = null

    private val bIsTracking = AtomicBoolean(true)
    private val bReCenterView = AtomicBoolean(false)
    private val bTouchDown = AtomicBoolean(false)
    private val bClearDrawing = AtomicBoolean(false)
    private val bLineParameters = AtomicBoolean(false)
    private val bUndo = AtomicBoolean(false)
    private val bNewStroke = AtomicBoolean(false)

    private var mStrokes: ArrayList<ArrayList<Vector3f>>? = null

    private var mDisplayRotationHelper: DisplayRotationHelper? = null
    private val mMessageSnackbar: Snackbar? = null

    private var bInstallRequested: Boolean = false

    private var mState: TrackingState? = null


    /**
     * Get a matrix usable for zero calibration (only position and compass direction)
     */
    val calibrationMatrix: FloatArray
        get() {
            val t = FloatArray(3)
            val m = FloatArray(16)

            mFrame!!.camera.pose.getTranslation(t, 0)
            val z = mFrame!!.camera.pose.zAxis
            val zAxis = Vector3f(z[0], z[1], z[2])
            zAxis.y = 0f
            zAxis.normalize()

            val rotate = Math.atan2(zAxis.x.toDouble(), zAxis.z.toDouble())

            Matrix.setIdentityM(m, 0)
            Matrix.translateM(m, 0, t[0], t[1], t[2])
            Matrix.rotateM(m, 0, Math.toDegrees(rotate).toFloat(), 0f, 1f, 0f)
            return m
        }

    /**
     * Setup the app when main activity is created
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)

        setContentView(R.layout.activity_main)

        mSurfaceView = findViewById(R.id.surfaceview)
        mSettingsUI = findViewById(R.id.strokeUI)
        mButtonBar = findViewById(R.id.button_bar)

        // Settings seek bars
        mLineDistanceScaleBar = findViewById(R.id.distanceScale)
        mLineWidthBar = findViewById(R.id.lineWidth)
        mSmoothingBar = findViewById(R.id.smoothingSeekBar)

        mLineDistanceScaleBar!!.progress = sharedPref.getInt("mLineDistanceScale", 1)
        mLineWidthBar!!.progress = sharedPref.getInt("mLineWidth", 10)
        mSmoothingBar!!.progress = sharedPref.getInt("mSmoothing", 50)

        mDistanceScale = LineUtils.map(mLineDistanceScaleBar!!.progress.toFloat(), 0f, 100f, 1f, 200f, true)
        mLineWidthMax = LineUtils.map(mLineWidthBar!!.progress.toFloat(), 0f, 100f, 0.1f, 5f, true)
        mLineSmoothing = LineUtils.map(mSmoothingBar!!.progress.toFloat(), 0f, 100f, 0.01f, 0.2f, true)

        val seekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
            /**
             * Listen for seekbar changes, and update the settings
             */
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                val editor = sharedPref.edit()

                if (seekBar === mLineDistanceScaleBar) {
                    editor.putInt("mLineDistanceScale", progress)
                    mDistanceScale = LineUtils.map(progress.toFloat(), 0f, 100f, 1f, 200f, true)
                } else if (seekBar === mLineWidthBar) {
                    editor.putInt("mLineWidth", progress)
                    mLineWidthMax = LineUtils.map(progress.toFloat(), 0f, 100f, 0.1f, 5f, true)
                } else if (seekBar === mSmoothingBar) {
                    editor.putInt("mSmoothing", progress)
                    mLineSmoothing = LineUtils.map(progress.toFloat(), 0f, 100f, 0.01f, 0.2f, true)
                }
                mLineShaderRenderer.bNeedsUpdate.set(true)

                editor.apply()

            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        }

        mLineDistanceScaleBar!!.setOnSeekBarChangeListener(seekBarChangeListener)
        mLineWidthBar!!.setOnSeekBarChangeListener(seekBarChangeListener)
        mSmoothingBar!!.setOnSeekBarChangeListener(seekBarChangeListener)

        // Hide the settings ui
        mSettingsUI!!.visibility = View.GONE

        mDisplayRotationHelper = DisplayRotationHelper(/*context=*/this)
        // Reset the zero matrix
        Matrix.setIdentityM(mZeroMatrix, 0)

        mLastPoint = Vector3f(0f, 0f, 0f)

        bInstallRequested = false

        // Set up renderer.
        mSurfaceView!!.preserveEGLContextOnPause = true
        mSurfaceView!!.setEGLContextClientVersion(2)
        mSurfaceView!!.setEGLConfigChooser(8, 8, 8, 8, 16, 0) // Alpha used for plane blending.
        mSurfaceView!!.setRenderer(this)
        mSurfaceView!!.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY

        // Setup touch detector
        mDetector = GestureDetectorCompat(this, this)
        mDetector!!.setOnDoubleTapListener(this)
        mStrokes = ArrayList()


    }


    /**
     * addStroke adds a new stroke to the scene
     *
     * @param touchPoint a 2D point in screen space and is projected into 3D world space
     */
    private fun addStroke(touchPoint: Vector2f) {
        val newPoint = LineUtils.GetWorldCoords(touchPoint, mScreenWidth, mScreenHeight, projmtx, viewmtx)
        addStroke(newPoint)
    }


    /**
     * addPoint adds a point to the current stroke
     *
     * @param touchPoint a 2D point in screen space and is projected into 3D world space
     */
    private fun addPoint(touchPoint: Vector2f) {
        val newPoint = LineUtils.GetWorldCoords(touchPoint, mScreenWidth, mScreenHeight, projmtx, viewmtx)
        addPoint(newPoint)
    }


    /**
     * addStroke creates a new stroke
     *
     * @param newPoint a 3D point in world space
     */
    private fun addStroke(newPoint:Vector3f) {
        biquadFilter = BiquadFilter(mLineSmoothing.toDouble())
        for (i in 0..1499)
        {
            biquadFilter!!.update(newPoint)
        }
        val p = biquadFilter!!.update(newPoint)
        mLastPoint = Vector3f(p)
        mStrokes!!.add(ArrayList<Vector3f>())
        mStrokes!!.get(mStrokes!!.size - 1).add(mLastPoint!!)
    }
    /*private fun addStroke(newPoint: Vector3f) {
        biquadFilter = BiquadFilter(mLineSmoothing.toDouble())
        for (i in 0..1499) {
            biquadFilter!!.update(newPoint)
        }
        val p = biquadFilter!!.update(newPoint)
        mLastPoint = Vector3f(p)
        mStrokes!!.add(ArrayList())
        mStrokes!![mStrokes!!.size - 1].add(mLastPoint)
    }*/

    /**
     * addPoint adds a point to the current stroke
     *
     * @param newPoint a 3D point in world space
     */

    private fun addPoint(newPoint:Vector3f) {
        if (LineUtils.distanceCheck(newPoint, mLastPoint))
        {
            val p = biquadFilter!!.update(newPoint)
            mLastPoint = Vector3f(p)
            mStrokes!!.get(mStrokes!!.size - 1).add(mLastPoint!!)
        }
    }
    /*private fun addPoint(newPoint: Vector3f) {
        if (LineUtils.distanceCheck(newPoint, mLastPoint)) {
            val p = biquadFilter!!.update(newPoint)
            mLastPoint = Vector3f(p)
            mStrokes!![mStrokes!!.size - 1].add(mLastPoint)
        }
    }*/


    /**
     * onResume part of the Android Activity Lifecycle
     */
    override fun onResume() {
        super.onResume()

        if (mSession == null) {
            var exception: Exception? = null
            var message: String? = null
            try {
                when (ArCoreApk.getInstance().requestInstall(this, !bInstallRequested)) {
                    ArCoreApk.InstallStatus.INSTALL_REQUESTED -> {
                        bInstallRequested = true
                        return
                    }
                    ArCoreApk.InstallStatus.INSTALLED -> {
                    }
                }

                // ARCore requires camera permissions to operate. If we did not yet obtain runtime
                // permission on Android M and above, now is a good time to ask the user for it.
                /*if (!PermissionHelper.hasCameraPermission(this)) {
                    PermissionHelper.requestCameraPermission(this);
                    return;
                }*/

                mSession = Session(/* context= */this)
            } catch (e: UnavailableArcoreNotInstalledException) {
                message = "Please install ARCore"
                exception = e
            } catch (e: UnavailableUserDeclinedInstallationException) {
                message = "Please install ARCore"
                exception = e
            } catch (e: UnavailableApkTooOldException) {
                message = "Please update ARCore"
                exception = e
            } catch (e: UnavailableSdkTooOldException) {
                message = "Please update this app"
                exception = e
            } catch (e: Exception) {
                message = "This device does not support AR"
                exception = e
            }

            if (message != null) {
                Log.e(TAG, "Exception creating session", exception)
                return
            }

            // Create default config and check if supported.
            val config = Config(mSession!!)
            if (!mSession!!.isSupported(config)) {
                Log.e(TAG, "Exception creating session Device Does Not Support ARCore", exception)
            }
            mSession!!.configure(config)
        }
        // Note that order matters - see the note in onPause(), the reverse applies here.
        mSession!!.resume()
        mSurfaceView!!.onResume()
        mDisplayRotationHelper!!.onResume()
        mPaused = false
    }

    /**
     * onPause part of the Android Activity Lifecycle
     */
    public override fun onPause() {
        super.onPause()
        // Note that the order matters - GLSurfaceView is paused first so that it does not try
        // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
        // still call mSession.update() and get a SessionPausedException.

        if (mSession != null) {
            mDisplayRotationHelper!!.onPause()
            mSurfaceView!!.onPause()
            mSession!!.pause()
        }

        mPaused = false


        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        mScreenHeight = displayMetrics.heightPixels.toFloat()
        mScreenWidth = displayMetrics.widthPixels.toFloat()
    }


    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (!PermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this,
                    "Camera permission is needed to run this application", Toast.LENGTH_LONG).show();
            finish();
        }
    }*/

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            // Standard Android full-screen functionality.
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }


    /**
     * Create renderers after the Surface is Created and on the GL Thread
     */
    override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f)

        // Create the texture and pass it to ARCore session to be filled during update().
        mBackgroundRenderer.createOnGlThread(/*context=*/this)
        mSession!!.setCameraTextureName(mBackgroundRenderer.textureId)
        mLineShaderRenderer.createOnGlThread(this)
    }


    override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
        // Notify ARCore session that the view size changed so that the perspective matrix and
        // the video background can be properly adjusted.
        mDisplayRotationHelper!!.onSurfaceChanged(width, height)
        mScreenWidth = width.toFloat()
        mScreenHeight = height.toFloat()
    }


    /**
     * update() is executed on the GL Thread.
     * The method handles all operations that need to take place before drawing to the screen.
     * The method :
     * extracts the current projection matrix and view matrix from the AR Pose
     * handles adding stroke and points to the data collections
     * updates the ZeroMatrix and performs the matrix multiplication needed to re-center the drawing
     * updates the Line Renderer with the current strokes, color, distance scale, line width etc
     */
    private fun update() {

        if (mSession == null) {
            return
        }

        mDisplayRotationHelper!!.updateSessionIfNeeded(mSession)

        try {

            mSession!!.setCameraTextureName(mBackgroundRenderer.textureId)

            mFrame = mSession!!.update()
            val camera = mFrame!!.camera

            mState = camera.trackingState

            // Update tracking states
            if (mState == TrackingState.TRACKING && !bIsTracking.get()) {
                bIsTracking.set(true)
            } else if (mState == TrackingState.STOPPED && bIsTracking.get()) {
                bIsTracking.set(false)
                bTouchDown.set(false)
            }

            // Get projection matrix.
            camera.getProjectionMatrix(projmtx, 0, AppSettings.getNearClip(), AppSettings.getFarClip())
            camera.getViewMatrix(viewmtx, 0)

            val position = FloatArray(3)
            camera.pose.getTranslation(position, 0)

            // Check if camera has moved much, if thats the case, stop touchDown events
            // (stop drawing lines abruptly through the air)
            if (mLastFramePosition != null) {
                val distance = Vector3f(position[0], position[1], position[2])
                distance.sub(Vector3f(mLastFramePosition!![0], mLastFramePosition!![1], mLastFramePosition!![2]))

                if (distance.length() > 0.15) {
                    bTouchDown.set(false)
                }
            }
            mLastFramePosition = position

            // Multiply the zero matrix
            Matrix.multiplyMM(viewmtx, 0, viewmtx, 0, mZeroMatrix, 0)


            if (bNewStroke.get()) {
                bNewStroke.set(false)
                addStroke(lastTouch.get())
                mLineShaderRenderer.bNeedsUpdate.set(true)
            } else if (bTouchDown.get()) {
                addPoint(lastTouch.get())
                mLineShaderRenderer.bNeedsUpdate.set(true)
            }

            if (bReCenterView.get()) {
                bReCenterView.set(false)
                mZeroMatrix = calibrationMatrix
            }

            if (bClearDrawing.get()) {
                bClearDrawing.set(false)
                clearDrawing()
                mLineShaderRenderer.bNeedsUpdate.set(true)
            }

            if (bUndo.get()) {
                bUndo.set(false)
                if (mStrokes!!.size > 0) {
                    mStrokes!!.removeAt(mStrokes!!.size - 1)
                    mLineShaderRenderer.bNeedsUpdate.set(true)
                }
            }
            mLineShaderRenderer.setDrawDebug(bLineParameters.get())
            if (mLineShaderRenderer.bNeedsUpdate.get()) {
                mLineShaderRenderer.setColor(AppSettings.getColor())
                mLineShaderRenderer.mDrawDistance = AppSettings.getStrokeDrawDistance()
                mLineShaderRenderer.setDistanceScale(mDistanceScale)
                mLineShaderRenderer.setLineWidth(mLineWidthMax)
                mLineShaderRenderer.clear()
                mLineShaderRenderer.updateStrokes(mStrokes)
                mLineShaderRenderer.upload()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    /**
     * GL Thread Loop
     * clears the Color Buffer and Depth Buffer, draws the current texture from the camera
     * and draws the Line Renderer if ARCore is tracking the world around it
     */
    override fun onDrawFrame(gl: GL10) {
        if (mPaused) return

        update()

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        // Draw background.
        mBackgroundRenderer.draw(mFrame)

        // Draw Lines
        if (mFrame!!.camera.trackingState == TrackingState.TRACKING) {
            mLineShaderRenderer.draw(viewmtx, projmtx, mScreenWidth, mScreenHeight, AppSettings.getNearClip(), AppSettings.getFarClip())
        }
    }


    /**
     * Clears the Datacollection of Strokes and sets the Line Renderer to clear and update itself
     * Designed to be executed on the GL Thread
     */
    fun clearDrawing() {
        mStrokes!!.clear()
        mLineShaderRenderer.clear()
    }


    /**
     * onClickUndo handles the touch input on the GUI and sets the AtomicBoolean bUndo to be true
     * the actual undo functionality is executed in the GL Thread
     */
    fun onClickUndo(button: View) {
        bUndo.set(true)
    }

    /**
     * onClickLineDebug toggles the Line Renderer's Debug View on and off. The line renderer will
     * highlight the lines on the same depth plane to allow users to draw things more coherently
     */
    fun onClickLineDebug(button: View) {
        bLineParameters.set(!bLineParameters.get())
    }


    /**
     * onClickSettings toggles showing and hiding the Line Width, Smoothing, and Debug View toggle
     */
    fun onClickSettings(button: View) {
        val settingsButton = findViewById<ImageButton>(R.id.settingsButton)

        if (mSettingsUI!!.visibility == View.GONE) {
            mSettingsUI!!.visibility = View.VISIBLE
            mLineDistanceScaleBar = findViewById(R.id.distanceScale)
            mLineWidthBar = findViewById(R.id.lineWidth)

            settingsButton.setColorFilter(resources.getColor(R.color.colorAccent))
        } else {
            mSettingsUI!!.visibility = View.GONE
            settingsButton.setColorFilter(resources.getColor(R.color.black))
        }
    }

    /**
     * onClickClear handle showing an AlertDialog to clear the drawing
     */
    fun onClickClear(button: View) {

        val builder = AlertDialog.Builder(this)
                .setMessage("Sure you want to clear?")

        // Set up the buttons
        builder.setPositiveButton("Clear ") { dialog, which -> bClearDrawing.set(true) }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

        builder.show()
    }


    /**
     * onClickRecenter handles the touch input on the GUI and sets the AtomicBoolean bReCEnterView to be true
     * the actual recenter functionality is executed on the GL Thread
     */
    fun onClickRecenter(button: View) {
        bReCenterView.set(true)
    }

    // ------- Touch events

    /**
     * onTouchEvent handles saving the lastTouch screen position and setting bTouchDown and bNewStroke
     * AtomicBooleans to trigger addPoint and addStroke on the GL Thread to be called
     */
    override fun onTouchEvent(tap: MotionEvent): Boolean {
        this.mDetector!!.onTouchEvent(tap)

        if (tap.action == MotionEvent.ACTION_DOWN) {
            lastTouch.set(Vector2f(tap.x, tap.y))
            bTouchDown.set(true)
            bNewStroke.set(true)
            return true
        } else if (tap.action == MotionEvent.ACTION_MOVE || tap.action == MotionEvent.ACTION_POINTER_DOWN) {
            lastTouch.set(Vector2f(tap.x, tap.y))
            bTouchDown.set(true)
            return true
        } else if (tap.action == MotionEvent.ACTION_UP || tap.action == MotionEvent.ACTION_CANCEL) {
            bTouchDown.set(false)
            lastTouch.set(Vector2f(tap.x, tap.y))
            return true
        }

        return super.onTouchEvent(tap)
    }


    override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
        return false
    }

    /**
     * onDoubleTap shows and hides the Button Bar at the Top of the View
     */
    override fun onDoubleTap(e: MotionEvent): Boolean {
        if (mButtonBar!!.visibility == View.GONE) {
            mButtonBar!!.visibility = View.VISIBLE
        } else {
            mButtonBar!!.visibility = View.GONE
        }
        return false
    }

    override fun onDoubleTapEvent(e: MotionEvent): Boolean {
        return false
    }

    override fun onDown(e: MotionEvent): Boolean {
        return false
    }

    override fun onShowPress(tap: MotionEvent) {}

    override fun onSingleTapUp(e: MotionEvent): Boolean {
        return false
    }

    override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent) {

    }

    override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
        return false
    }

    companion object {
        private val TAG = DrawAR::class.java.simpleName
    }

}
