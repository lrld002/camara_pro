
@file:JvmName("Constants")
package com.android.sgifer.camarapro

import android.Manifest

/**
 * Created by SGIFER on 15/03/18.
 */

val REQUEST_VIDEO_PERMISSIONS = 1
val VIDEO_PERMISSIONS = arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
