package com.android.sgifer.camarapro

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import com.google.ar.core.ArCoreApk
import com.google.ar.core.Session
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException

class CameraActivity : AppCompatActivity() {

    private var mSession: Session? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        savedInstanceState ?: supportFragmentManager.beginTransaction()
                .replace(R.id.container, CameraFragment.newInstance())
                .commit()
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    override fun onResume() {
        super.onResume()
        var mUserRequestedInstall = true
        try
        {
            if (mSession == null)
            {
                /*when (ArCoreApk.getInstance().requestInstall(this, mUserRequestedInstall)) {
                    INSTALLED
                    INSTALLED -> mSession = Session(this)
                    INSTALL_REQUESTED -> {
                        // Ensures next invocation of requestInstall() will either return
                        // INSTALLED or throw an exception.
                        mUserRequestedInstall = false
                        return
                    }
                }// Success.*/
                when (ArCoreApk.getInstance().requestInstall(this,mUserRequestedInstall)){

                    ArCoreApk.InstallStatus.INSTALLED -> {mSession
                        Toast.makeText(this,"instalado", Toast.LENGTH_SHORT).show()}
                    ArCoreApk.InstallStatus.INSTALL_REQUESTED -> {mUserRequestedInstall = false
                        return
                    }
                }
            }
        }
        catch (e: UnavailableUserDeclinedInstallationException) {
            // Display an appropriate message to the user and return gracefully.
            return
        }
        run({ // current catch statements
            return // mSession is still null
        })
    }
}
