package com.android.sgifer.camarapro.rendering;

/**
 * Created by SGIFER on 23/03/18.
 */

import javax.vecmath.Vector3f;

public class Ray {
    public final Vector3f origin;
    public final Vector3f direction;
    public Ray(Vector3f origin, Vector3f direction) {
        this.origin = origin;
        this.direction = direction;
    }
}
